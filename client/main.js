import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import {Meteor} from 'meteor/meteor';

// import HTML
import './main.html';
import './html/index.html';
import './html/tapbar.html';
import './html/createdoc.html';
import './html/runingdoc.html';
import './html/searchdoc.html';
import './html/typedoc.html';
import './html/contact.html';

// import CSS
import './css/main.css';
import './css/tapbar.css';

// import JS
import '../lib/collection/tabular.js';
import '../lib/routes/ironRouter.js';
import './js/tapbar.js';
import './js/createdoc.js';
import './js/runingdoc.js';
import './js/searchdoc.js';
import './js/typedoc.js';
import './js/index.js';

Template.login.events({
    'click #signinbtn': function(e){
      var username = document.getElementById("user").value;
	  var password = document.getElementById("pass").value;
	  var newUser = "Thira";
	  var newPass = "Thira0826";
	  Meteor.loginWithPassword(username, password, function(err){
		if(err) {
			alert("Account is not found!!!");
			return false;
		} else {
			Router.go("/index");
		}
	});
	  return false; 
    }
});


Template.signup.events({
    'click #signupbtn': function(e){
		var pos = trimInput(document.getElementById("tpos").value);
		var tpermis = trimInput(document.getElementById("tpermis").value);
		var toffice = trimInput(document.getElementById("toffice").value);
		var name = trimInput(document.getElementById("name").value);
      	var email = trimInput(document.getElementById("user").value);
		var password = trimInput(document.getElementById("pass").value);
		//   var password2 = trimInput(document.getElementById("pass2").value);
		// alert(pos);
		Accounts.createUser({
			profile: {position:pos, permis:tpermis, office:toffice},
			username: name,
			email: email,
			password: password,
			//Roles.addUsersToRoles(Meteor.userId, 'admin', null)
		});
		//Roles.addUsersToRoles(Meteor.userId, 'admin', null);
		Router.go("/index");
		return false;
    }
});

// // Validation Rules

// // Trim Helper
var trimInput = function(val){
	return val.replace(/^\s*|\s*$/g, "");
};

// var isNotEmpty = function(value){
// 	if (value && value !== ''){
// 		return true;
// 	}
// 	Bert.alert("Please fill in all fields", "danger", "growl-top-right");
// 	return false;
// };

// // Validate Email
// isEmail = function(value) {
// 	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
// 	if(filter.test(value)) {
// 		return true;
// 	}
// 	Bert.alert("Please use a valid email address", "danger", "growl-top-right");
// 	return false;
// };

// // Check Password Field
// isValidPassword = function(password){
// 	if(password.length <6) {
// 		Bert.alert("Password must be at least 6 characters", "danger", "growl-top-right");
// 		return false;
// 	}
// 	return true;
// };

// // Match Password
// areValidPasswords = function(password, confirm) {
// 	if(!isValidPassword(password)) {
// 		return false;
// 	}
// 	if(password !== confirm) {
// 		Bert.alert("Passwords do not match", "danger", "growl-top-right");
// 		return false;
// 	}
// 	return true;
// };


// Template.hello.onCreated(function helloOnCreated() {
//   // counter starts at 0
//   this.counter = new ReactiveVar(0);
// });

// Template.hello.helpers({
//   counter() {
//     return Template.instance().counter.get();
//   },
// });

// Template.hello.events({
//   'click button'(event, instance) {
//     // increment the counter when button is clicked
//     instance.counter.set(instance.counter.get() + 1);
//   },
// });

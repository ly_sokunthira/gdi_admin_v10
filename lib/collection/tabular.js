import Tabular from 'meteor/aldeed:tabular';
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Ksp } from './createdocdb.js';
import { Historys } from './runingdocdb.js';

export const TabularTables = {};

TabularTables.Ksp = new Tabular.Table({
        name: "Ksp",
        collection: Ksp,
        responsive: true,
        bFilter: true,
        stateSave: true,
        autoWidth: false,
        columns: [

                {data: 'txtcodedoc', title: 'លេខកូដ' },
                {data: 'txtdatein', title: 'កាលបរិច្ឆេទ' },
                {data: 'txtdatedoc', title: 'កាលបរិច្ឆេទឯកសារ' },
                {data: 'txtdescription', title: 'បរិយាយ' },
                {
                        data: "_id", width:'200px', orderable: false, className: "text-center",
                        title: "កែសម្រួល",
                        render: function (e){
                                var btntxt = new Spacebars.SafeString("<a href='' id='edit_asset' class='btn btn-info btn-xs'><i class='fa fa-edit'></i> Change</a> ");
                                return btntxt + new Spacebars.SafeString("<a href='' id='delete_asset' class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</a>");
                        }
                }
        ]
});

TabularTables.Historys = new Tabular.Table({
        name: "Historys",
        collection: Historys,
        responsive: true,
        bFilter: true,
        stateSave: true,
        autoWidth: false,
        columns: [

                {data: 'txtcodedoc', title: 'លេខកូដ' },
                {data: 'txtdatein', title: 'កាលបរិច្ឆេទ' },
                {data: 'txtdatedoc', title: 'កាលបរិច្ឆេទឯកសារ' },
                {data: 'txtdescription', title: 'បរិយាយ' },
                {
                        data: "_id", width:'200px', orderable: false, className: "text-center",
                        title: "កែសម្រួល",
                        render: function (e){
                                var btntxt = new Spacebars.SafeString("<a href='' id='edit_asset' class='btn btn-info btn-xs'><i class='fa fa-edit'></i> Change</a> ");
                                return btntxt + new Spacebars.SafeString("<a href='' id='delete_asset' class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</a>");
                        }
                }
        ]
});
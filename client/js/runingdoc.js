import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { Historys } from '../../lib/collection/runingdocdb.js';
import { Ksp} from '../../lib/collection/createdocdb.js';
import '../html/runingdoc.html';
import { TabularTables } from '../../lib/collection/tabular.js';

Meteor.subscribe('historys');
Meteor.subscribe('ksp');
// Session.setDefault('skip', 0);
// Session.setDefault('FilterText', '');
// Tracker.autorun(() => {
//     Meteor.subscribe('ksp', Session.get('FilterText'), Session.get('skip'));
// });
Template.rundoc.events({
    'click #sent'(e){
        e.preventDefault();
        var txtid= e.currentTarget.dataset.postId;
        var codedoc = this.txtcodedoc;
        var datein = this.txtdatein;
        var datedoc = this.txtdatedoc;
        var create = this.txtcreate;
        var description = this.txtdescription;
        var tsent = Meteor.user().profile.position;
        var txtruningdoc = document.getElementById("truning").value;
        var txtdatesend = document.getElementById("tdatesend").value;
        var txtreceive = document.getElementById("treceive").value;
        var txtnote = document.getElementById("tnote").value;
        var tstate = '1';
        
        if (txtruningdoc == '' || txtdatesend=='' || txtreceive==''){
            alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ.....");
        }
        else {
            if(confirm('តើលោកអ្នកពិតជាចង់បញ្ជូនឯកសារនេះមែនឬទេ?')){
                // alert(txtid,codedoc,datein,datedoc,create,description);
                Meteor.call("run_sent",txtid,codedoc,datein,datedoc,create,description,tsent,txtruningdoc,txtdatesend,txtreceive,txtnote,tstate);
                Historys.insert({
                  txtid: this._id,
                  txtcodedoc: this.txtcodedoc,
                  txtdatein: this.txtdatein,
                  txtdatedoc: this.txtdatedoc,
                  txtcreate: this.txtcreate,
                  txtdescription: this.txtdescription,
                  txtsent:tsent,
                  txtuser: txtruningdoc,
                  txtdatesend: txtdatesend,
                  txtreceive: txtreceive,
                  txtnote: txtnote,
                  txtstate:tstate
                }); 
                alert("ការបញ្ជូនទទួលបានជោគជ័យ");
                Ksp.update(this._id,{$set:{txtuser:txtruningdoc,txtstate:tstate}});
                document.getElementById("truning").value='';
                document.getElementById("tdatesend").value='';
                document.getElementById("treceive").value='';
            }
        }    
    }
    // 'click #previousData1' (e){
    //     if(Session.get('skip') >= 10){
    //         Session.set('skip', Session.get('skip') - 10);
    //     }
    // },
    // 'click #nextData1' (e){
    //     if(Session.get('skip') <= 10){
    //         Session.set('skip', Session.get('skip') + 10);
    //     }
    // },
    // 'keyup #sSearch' (e){
    //     var dInput = document.getElementById('sSearch').value;
    //     // console.log(dInput);
    //     Session.set('FilterText', dInput);
    // }
});
  
Template.rundoc.helpers({
    ksp(){
      return Ksp.find({ txtuser: Meteor.user().profile.position },{sort:{txtcodedoc:-1}});
    },
    // historys(){
    //   return Historys.find({});
    // },
    Ksp: function (event){
        return TabularTables.Ksp;
    }
});
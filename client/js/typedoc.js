import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import { Typedocs } from '../../lib/collection/typedocdb.js';
import '../html/typedoc.html';
Meteor.subscribe('typedocs');

Template.typedoc.events({
    'click #inputtypedoc'(e) {
        e.preventDefault();
        var code = document.getElementById("tcode").value;
        var type = document.getElementById("ttype").value;
        // alert(txtcode);
        if (code == '' || type=='' ){
            alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ!!!...");
        }
        else {
            Meteor.call("createtypedocs",code,type);  
            // Clear form
            document.getElementById("tcode").value = '';
            document.getElementById("ttype").value = '';
            alert("ទិន្នន័យបញ្ចូលរួចរាល់ហើយ!!!..."); 
        }
    }
});

Template.datagridtypedoc.events({
  'click #editme'(e){
      e.preventDefault();
      var postId = e.currentTarget.dataset.postId;
      var Ucode = document.getElementById("tcode").value;
      var Utype = document.getElementById("ttype").value;
      if (Ucode == '' || Utype=='' ){
          alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ!!!...");
      }
      else {
          Meteor.call("edittypedocs",postId,Ucode,Utype)
          document.getElementById("tcode").value='';
          document.getElementById("ttype").value='';
          alert("ទិន្នន័យកែសម្រួលរួចរាល់ហើយ!!!...");
      }
  }
});

Template.datagridtypedoc.helpers({
    typedocs(){
        return Typedocs.find();
    }
});
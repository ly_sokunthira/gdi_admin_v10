import { Meteor } from 'meteor/meteor';
import { Typedocs } from '../lib/collection/typedocdb.js';
import { Ksp } from '../lib/collection/createdocdb.js';
import { Historys }  from '../lib/collection/runingdocdb.js';
import '../lib/collection/tabular.js';


Meteor.publish('typedocs', function () {
  return Typedocs.find({});
});
Meteor.publish('ksp', function () {
    return Ksp.find({});
});
Meteor.publish('historys', function () {
    return Historys.find({});
});

//Typedocs Code
Meteor.methods({
    'createtypedocs': function (code,type) {
        Typedocs.insert({
            txtcode: code,
            txttype: type
        }); 
    },
    'edittypedocs': function(typeID,Ucode,Utype){
        Typedocs.update(typeID,{$set:{txtcode:Ucode,
            txttype:Utype}
        });
    }
});

//Create Document Code
Meteor.methods({
    'createdoc': function (codedoc,datein,datedoc,create,description,user,state) {
        Ksp.insert({
            txtcodedoc: codedoc,
            txtdatein: datein,
            txtdatedoc: datedoc,
            txtcreate: create,
            txtdescription: description,
            txtuser: user,
            txtstate: state,
            txttimein: new Date().toLocaleString()
        });
    },
    'doc_remove': function(docID){
        // check(docID,String);
        Ksp.remove(docID);
    },
    'doc_edit': function(docID,codedoc,datein,datedoc,create,description,user,state){
        Ksp.update(docID,{$set:{
            txtcodedoc: codedoc,
            txtdatein: datein,
            txtdatedoc: datedoc,
            txtcreate: create,
            txtdescription: description,
            txtuser: user,
            txtstate: state}
        });
    }
});

//Running Document Code
Meteor.methods({
    'run_sent': function (docID,codedoc,datein,datedoc,create,description,sent,rundoc,datesend,receive,note,state){
        Historys.insert({
            txtid: docID,
            txtcodedoc: codedoc,
            txtdatein: datein,
            txtdatedoc: datedoc,
            txtcreate: create,
            txtdescription: description,
            txtsent: sent,
            txtuser: rundoc,
            txtdatesend: datesend,
            txtreceive: receive,
            txtnote: note,
            txtstate: state
        }); 
        Ksp.update(docID,{$set:{
            txtuser: rundoc,
            txtstate: state
        }});
    }
});
// Report Document

import '../../client/appLayout.html';
Router.configure({ 
  layoutTemplate : 'appLayout' 
}); 

Router.route('/', function () { 
  this.render('login'); 
});  
Router.route('/appLayout', function () {
  if(Meteor.user()){
    this.render('appLayout');
  } 
});  

Router.route('/signup', function () {
  this.render('signup'); 
});
Router.route('/index', function () { 
  this.render('index'); 
});
Router.route('/searchdoc', function () { 
  if(Meteor.user()){
  this.render('searchdoc');
  } 
});
Router.route('/createdoc', function () { 
  if(Meteor.user()){
  this.render('createdoc');
  } 
});
Router.route('/rundoc', function () { 
  if(Meteor.user()){
  this.render('rundoc');
  } 
});
Router.route('/typedoc', function () { 
  if(Meteor.user()){
  this.render('typedoc');
  } 
});
Router.route('/contact', function () { 
  if(Meteor.user()){
  this.render('contact');
  } 
});
// Router.route('/admin', function () { 
//   if(Meteor.user()){
//   this.render('admin');
//   } 
// });
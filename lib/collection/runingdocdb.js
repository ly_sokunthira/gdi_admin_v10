import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
export const Historys = new Mongo.Collection('historys');
const Schemas = {};
Schemas.Historys = new SimpleSchema({
    txtid: {
        type: String,
        label: "txtid",
        max: 200
    },
    txtcodedoc: {
        type: String,
        label: "txtcodedoc",
        max: 200
    },
    txtdatein: {
        type: Date,
        label: "txtdatein"
    },
    txtdatedoc: {
        type: Date,
        label: "txtdatedoc"
    },
    txtcreate: {
        type: String,
        label: "txtcreate",
        max: 200
    },
    txtdescription: {
        type: String,
        label: "txtdescription"
    },
    txttimein: {
        type: Date,
        label: "txttimein"
    },
    txtsent: {
        type: String,
        label: "txtsent",
        max: 200
    },
    txtuser: {
        type: String,
        label: "txtuser",
        max: 200
    },
    txtdatesend: {
        type: Date,
        label: "txtdatedoc"
    },
    txtreceive: {
        type: String,
        label: "txtreceive",
        max: 200
    },
    txtnote: {
        type: String,
        label: "txtnote",
        max: 200
    },
    txtstate: {
        type: String,
        label: "txtstate",
        max: 200
    }
});
Historys.attachSchema(Schemas.Historys);
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { Ksp } from '../../lib/collection/createdocdb.js';
import { Typedocs } from '../../lib/collection/typedocdb.js';
import '../html/createdoc.html';
import { TabularTables } from '../../lib/collection/tabular.js';

Meteor.subscribe('typedocs');
Meteor.subscribe('Ksp');
// Session.setDefault('skip', 0);
// Session.setDefault('FilterText', '');
// Tracker.autorun(() => {
//     Meteor.subscribe('ksp', Session.get('FilterText'), Session.get('skip'));
// });
Template.createdoc.events({
    'click #inputdoc' (e) {
        e.preventDefault();
        var txtcodedoc = document.getElementById("tcodedoc").value;
        var txttypedoc = document.getElementById("ttypedoc").value;
        var codedoc = txtcodedoc+txttypedoc;
        var datein = document.getElementById("tdatein").value;
        var datedoc = document.getElementById("tdatedoc").value;
        var create = document.getElementById("tcreate").value;
        var description = document.getElementById("tdescription").value;
        var user = Meteor.user().profile.position;
        var state = "0";
        if (txtcodedoc == '' || txttypedoc=='' || datein==''){
            alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ.....");
        }
        else if(txtcodedoc == this.txtcodedoc){
            alert("សូមពិនិត្យម្តងទៀត ទិន្នន័យមានរួចហើយ!!!!");
        }
        else{
            Meteor.call("createdoc",codedoc,datein,datedoc,create,description,user,state);
            // Clear form
            document.getElementById("tcodedoc").value = '';
            document.getElementById("ttypedoc").value = '';
            document.getElementById("tdatein").value = '';
            document.getElementById("tdatedoc").value= '';
            document.getElementById("tcreate").value = '';
            document.getElementById("tdescription").value = '';
            alert("ទិន្នន័យបញ្ចូលរួចរាល់ហើយ!!!...");
        }
    }
});
Template.datagridnew.events({
    'click #deletedoc'(e){
        e.preventDefault();
        var userPermission = document.getElementById("permis1").getAttribute('value');
        var postId = e.currentTarget.dataset.postId;
        if(userPermission != 'Admin' && userPermission != 'Manage'){
            alert("អ្នកជា "+ userPermission +" គ្មានសិទ្ធក្នុងការដែលសម្រួល"); 
            return;
        }
        else{
            if(confirm('តើអ្នកចង់លុបទិន្នន័យមែនឬទេ?')){
                Meteor.call("doc_remove",postId);
            }
        }
    },
    'click #editdoc' (e){
        e.preventDefault();
        var userPermission = document.getElementById("permis1").getAttribute('value');
        
        var postId = e.currentTarget.dataset.postId;
        var Ucodedoc = document.getElementById("tcodedoc").value;
        var Utypedoc = document.getElementById("ttypedoc").value;
        var codedoc = Ucodedoc+Utypedoc;
        var datein = document.getElementById("tdatein").value;
        var datedoc = document.getElementById("tdatedoc").value;
        var create = document.getElementById("tcreate").value;
        var description = document.getElementById("tdescription").value;
        var user = Meteor.user().profile.position;
        var state = "0";
        alert(userPermission);
        if (codedoc == '' || Utypedoc=='' || datein=='' || datedoc==''||  create==''|| description==''){
            alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ និងកាលបរិច្ឆេទ!!!...");
        }
        else {
            if(userPermission != 'Admin' && userPermission != 'Manage'){
                alert("អ្នកជា "+ userPermission +" គ្មានសិទ្ធក្នុងការដែលសម្រួល"); 
                return;
            }
            else{
                Meteor.call("doc_edit",postId,codedoc,datein,datedoc,create,description,user,state);
                document.getElementById("tcodedoc").value='';
                document.getElementById("ttypedoc").value='';
                document.getElementById("tdatein").value='';
                document.getElementById("tdatedoc").value='';
                document.getElementById("tcreate").value='';
                document.getElementById("tdescription").value='';
                alert("ទិន្នន័យកែសម្រួលរួចរាល់ហើយ!!!...");
            }
        }
    }
    // 'click #previousData' (e){
    //     if(Session.get('skip') >= 10){
    //         Session.set('skip', Session.get('skip') - 10);
    //     }
    // },
    // 'click #nextData' (e){
    //     if(Session.get('skip') <= 10){
    //         Session.set('skip', Session.get('skip') + 10);
    //     }
    // },
    // 'keyup #sSearch' (e){
    //     var dInput = document.getElementById('sSearch').value;
    //     // console.log(dInput);
    //     Session.set('FilterText', dInput);
    // }
});

Template.createdoc.helpers({
    typedocs(){
        return Typedocs.find({});
    }
});

Template.datagridnew.helpers({
    ksp(){
        return Ksp.find({});
    },
    Ksp: function (event){
        return TabularTables.Ksp;
    }
});
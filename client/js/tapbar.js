import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Accounts } from 'meteor/accounts-base';
import '../html/tapbar.html';

Template.tapbar.events({
	"click .logout": function(event){
		Meteor.logout(function(err){
			Router.go('/');
			// if(err) {
			// 	Bert.alert(err.reason, "danger", "growl-top-right");
			// } else {
			// 	Router.go('/');
			// 	Bert.alert("you Are Now Logged Out", "success", "growl-top-right");
			// }
		});
	},
	"keyup #myInput": function(e){
		var value = document.getElementById('myInput').value;
		$("#myTable tr").filter(function() {
		   $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	}

});

Template.registerHelper("username", function() {
	//   return Meteor.user().emails[0].address;
	return Meteor.user().username;
	
});